<?php

namespace AppBundle\Services;

class DateHelperService
{
    public function dateDiff($startDate, $endDate)
    {
        try {
            $startDate = new \DateTime($startDate);
            $endDate = new \DateTime($endDate);
            $diff = $startDate->diff($endDate);
            return $diff->days + 1;
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function getWeekdayNumber($date)
    {
        return date('N', strtotime($date));
    }

    public function getWorkingDaysBeforeDay($dayNumber)
    {
        return $dayNumber > 5 ? 5 : $dayNumber;
    }

    public function getDaysAfterDay($dayNumber)
    {
        return (7 - $dayNumber + 1);
    }

    public function getWorkingDaysAfterDay($dayNumber)
    {
        return $dayNumber > 5 ? 0 : (5 - $dayNumber + 1);
    }

    public function getAllWorkingDays($daysCount)
    {
        return (($daysCount / 7) * 5);
    }

    public function isWorkingDay($date)
    {
        $dayNumber = $this->getWeekdayNumber($date);
        return $dayNumber < 6;
    }
}
