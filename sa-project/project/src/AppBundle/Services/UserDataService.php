<?php


namespace AppBundle\Services;


class UserDataService
{
    public static function getData()
    {
        return [
            ['id' => 1, 'name' => 'Oliver'],
            ['id' => 2, 'name' => 'Harry'],
            ['id' => 3, 'name' => 'George'],
            ['id' => 4, 'name' => 'Noah'],
            ['id' => 5, 'name' => 'Jack'],
            ['id' => 6, 'name' => 'Jacob'],
            ['id' => 7, 'name' => 'Leo'],
            ['id' => 8, 'name' => 'Oscar'],
            ['id' => 9, 'name' => 'Charlie'],
            ['id' => 10, 'name' => 'Muhammad'],
        ];
    }
}