<?php

namespace AppBundle\Services;

class WorkingDaysCounterService
{
    public $dateHelper;

    public function __construct(DateHelperService $dateHelper)
    {
        $this->dateHelper = $dateHelper;
    }

    public function calculate($startDate, $endDate, $holidays = [])
    {
        $daysCount = $this->dateHelper->dateDiff($startDate, $endDate);

        $startDayNumber = $this->dateHelper->getWeekdayNumber($startDate);
        $endDayNumber = $this->dateHelper->getWeekdayNumber($endDate);

        //cut days of first week and days of last to became only full weeks
        $daysCount -= $this->dateHelper->getDaysAfterDay($startDayNumber);
        $daysCount -= $endDayNumber;

        $workingDaysCount = $this->dateHelper->getWorkingDaysAfterDay($startDayNumber) +
            $this->dateHelper->getAllWorkingDays($daysCount) +
            $this->dateHelper->getWorkingDaysBeforeDay($endDayNumber);

        foreach ($holidays as $holiday) {
            if ($this->dateHelper->isWorkingDay($holiday)) {
                $workingDaysCount--;
            }
        }
        return $workingDaysCount;
    }
}
