<?php


namespace AppBundle\Services;

class FinderService
{
    public $data;

    public function findAll()
    {
        if (!is_array($this->data)) {
            throw new \Exception;
        }

        return $this->data;
    }

    public function findOneById($id)
    {
        $data = $this->findAll();

        foreach ($data as $row) {
            if ($row['id'] === $id) {
                return $row;
            }
        }

        return null;
    }

    public function findOneBySlug($slug, $column)
    {
        $data = $this->findAll();
        $key = array_search($slug, array_column($data, $column));

        return $data[$key];
    }
}
