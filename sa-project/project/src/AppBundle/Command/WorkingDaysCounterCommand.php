<?php

namespace AppBundle\Command;

use AppBundle\Services\WorkingDaysCounterService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WorkingDaysCounterCommand extends ContainerAwareCommand
{
    public $counter;

    public function __construct(WorkingDaysCounterService $counter)
    {
        parent::__construct();
        $this->counter = $counter;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:working_days_counter_command')
            ->setDescription('I will count working days between two dates')
            ->setHelp('This command will not give you more holidays, sorry...')
            ->addArgument('startDate', InputArgument::REQUIRED, 'Start date\"Y-m-d\".')
            ->addArgument('endDate', InputArgument::REQUIRED, 'End date\"Y-m-d\".')
            ->addArgument('holidays', InputArgument::OPTIONAL, 'Holidays');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $startDate = $input->getArgument('startDate');
        $endDate = $input->getArgument('endDate');

        $holidays = $input->getArgument('holidays');
        $holidays = empty($holidays) ? [] : explode(',', $holidays);

        $output->writeln($this->counter->calculate($startDate, $endDate, $holidays));
    }
}
