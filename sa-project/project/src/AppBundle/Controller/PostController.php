<?php


namespace AppBundle\Controller;

use AppBundle\Services\FinderService;
use AppBundle\Services\PostDataService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package AppBundle\Controller
 * @Route("/post", name="post_")
 */
class PostController extends Controller
{
    public $finderService;

    public function __construct(
        FinderService $finderService,
        PostDataService $postDataService
    )
    {
        $data = $postDataService::getData();
        $finderService->data = $data;
        $this->finderService = $finderService;
    }
    /**
     * @Route(
     *     value="/{pageId}",
     *     name="list",
     *     requirements={"pageId"="\d+"},
     *     defaults={"pageId":null}
     * )
     * @param $pageId
     * @return Response
     * @throws \Exception
     */
    public function listAction($pageId)
    {
        $list = $this->finderService->findAll();
        return $this->render('post/list.html.twig', ['list' => $list]);
    }

    /**
     * @Route(
     *     value="/{slug}",
     *     name="view",
     *     requirements={"slug"="[a-zA-Z-]+"}
     * )
     * @param $slug
     * @return mixed
     */
    public function viewAction($slug)
    {
        $post = $this->finderService->findOneBySlug($slug, 'title');

        if ($post == null) {
            throw $this->createNotFoundException();
        }

        $this->render('post/show.html.twig', ['post' => $post]);
    }

}
