<?php

namespace AppBundle\Controller;

use AppBundle\Services\WorkingDaysCounterService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="homepage",
     *     host="{domain}",
     *     requirements={
     *         "domain"="%domain%"
     *     })
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/count", name="count-working-days")
     */
    public function testAction()
    {
        $workingDaysCounter = $this->container->get(WorkingdaysCounterService::class);
        $startDate = "2010-01-01";
        $endDate = "2019-01-01";
        $startTime = microtime(true);
        $workingDaysCount = $workingDaysCounter->calculate($startDate, $endDate);
        $scriptWorkTime = (microtime(true) - $startTime) * 1000;
        return $this->render('default/working-days.html.twig', [
            'workingDaysCount' => $workingDaysCount,
            'scriptWorkTime' => $scriptWorkTime,
        ]);
    }

}
