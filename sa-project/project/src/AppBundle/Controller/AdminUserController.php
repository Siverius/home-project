<?php

namespace AppBundle\Controller;

use AppBundle\Services\FinderService;
use AppBundle\Services\UserDataService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AdminUserController
 * @package AppBundle\Controller
 * @Route(
 *     value="/",
 *     name="admin_",
 *     requirements={
 *         "admin_subdomain"="%admin_subdomain%",
 *         "domain"="%domain%"
 *     },
 *     defaults={
 *         "admin_subdomain"="%admin_subdomain%",
 *         "domain"="%domain%"
 *     },
 *     host="{admin_subdomain}.{domain}"
 * )
 */
class AdminUserController extends Controller
{
    public $finderService;
    public $translator;

    public function __construct(
        FinderService $finderService,
        UserDataService $userDataService,
        TranslatorInterface $translator
    )
    {
        $data = $userDataService::getData();
        $finderService->data = $data;
        $this->finderService = $finderService;
        $this->translator = $translator;
    }

    /**
     * @Route(
     *     "/new",
     *     name="user_new",
     *     methods={"POST", "GET"}
     * )
     */
    public function newAction()
    {
        $message = $this->translator->trans('page_user_new');
        return $this->render('user/new.html.twig',
            [
                'message' => $message,
            ]
        );
    }


    /**
     * @Route(
     *     "/{page}/{pageId}",
     *     name="user_index",
     *     requirements={
     *         "page"="[a-zA-Z-]+",
     *         "pageId"="\d+"
     *     },
     *     methods={"GET"},
     *     defaults={
     *         "page"=null,
     *         "pageId"=null
     *     }
     * )
     * @param $page
     * @param $pageId
     * @return Response
     * @throws \Exception
     */
    public function indexAction($page = null, $pageId = null)
    {
        $users = $this->finderService->findAll();
        return $this->render('user/index.html.twig',
            [
                'users' => $users,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}",
     *     name="user_show",
     *     requirements={
     *         "id"="\d+"
     *     },
     *     methods={"GET"}
     * )
     * @param $id
     * @return mixed
     */
    public function showAction($id)
    {
        $user = $this->finderService->findOneById($id);

        if ($user == null) {
            throw $this->createNotFoundException();
        }

        return $this->render('user/show.html.twig', ['user' => $user]);
    }

    /**
     * @Route(
     *     "/{id}/edit",
     *     name="user_id_edit",
     *     requirements={
     *         "id"="\d+"
     *     },
     *     methods={
     *         "POST",
     *         "GET"
     *     }
     * )
     * @param $id
     * @return mixed
     */
    public function editAction($id)
    {
        $user = $this->finderService->findOneById($id);

        if ($user == null) {
            throw $this->createNotFoundException();
        }

        return $this->render('user/edit.html.twig', ['user' => $user]);
    }

    /**
     * @Route(
     *     "/{id}/delete",
     *     name="user_delete",
     *     requirements={
     *         "id"="\d+"
     *     },
     *     methods={"DELETE"}
     * )
     * @param $id
     * @return mixed
     */
    public function deleteAction($id)
    {
        $user = $this->finderService->findOneById($id);

        if ($user == null) {
            throw $this->createNotFoundException();
        }

        $this->addFlash(
            'info',
            $this->translator->trans("flash_admin_user_delete %name%", ['%title%'=>$user['name']])
        );

        return $this->redirectToRoute('admin_user_index');
    }
}
