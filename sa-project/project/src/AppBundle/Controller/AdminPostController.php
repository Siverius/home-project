<?php

namespace AppBundle\Controller;

use AppBundle\Services\FinderService;
use AppBundle\Services\PostDataService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class PostController
 * @package AppBundle\Controller
 * @Route(
 *     value="/user",
 *     name="admin_",
 *     host="{admin_subdomain}.{domain}",
 *     requirements={
 *         "admin_subdomain"="%admin_subdomain%",
 *         "domain"="%domain%"
 *     },
 *     defaults={
 *         "admin_subdomain"="%admin_subdomain%",
 *         "domain"="%domain%"
 *     }
 * )
 */

class AdminPostController extends Controller
{
    public $finderService;
    public $translator;

    public function __construct(
        FinderService $finderService,
        PostDataService $postDataService,
        TranslatorInterface $translator
    )
    {
        $data = $postDataService::getData();
        $finderService->data = $data;
        $this->finderService = $finderService;
        $this->translator = $translator;
    }

    /**
     * @Route(
     *     "/new",
     *     name="post_new",
     *     methods={
     *         "POST",
     *         "GET"
     *     }
     * )
     */
    public function newAction()
    {
        $message = $this->translator->trans('page_post_new');
        return $this->render('post/new.html.twig',
            [
                'message' => $message,
            ]
        );
    }

    /**
     * @Route(
     *     "/{page}/{pageId}",
     *     name="post_index",
     *     requirements={
     *         "page"="[a-zA-Z-]+",
     *         "pageId"="\d+"
     *     },
     *     defaults={
     *         "page"=null,
     *         "pageId"=null
     *     },
     *     methods={"GET"}
     * )
     * @param $page
     * @param $pageId
     * @return Response
     * @throws \Exception
     */
    public function indexAction($page = null, $pageId = null)
    {
        $posts = $this->finderService->findAll();
        return $this->render('post/index.html.twig',
            [
                'posts' => $posts,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}",
     *     name="post_show",
     *     requirements={
     *         "id"="\d+"
     *     },
     *     methods={"GET"}
     * )
     * @param $id
     * @return mixed
     */
    public function showAction($id)
    {
        $post = $this->finderService->findOneById($id);

        if ($post == null) {
            throw $this->createNotFoundException();
        }

        return $this->render('post/show.html.twig', ['post' => $post]);
    }

    /**
     * @Route(
     *     "/{id}/edit",
     *     name="post_id_edit",
     *     requirements={
     *         "id"="\d+"
     *     },
     *     methods={
     *         "POST",
     *         "GET"
     *     }
     * )
     * @param $id
     * @return mixed
     */
    public function editAction($id)
    {
        $post = $this->finderService->findOneById($id);

        if ($post == null) {
            throw $this->createNotFoundException();
        }

        return $this->render('post/edit.html.twig', ['post' => $post]);
    }

    /**
     * @Route(
     *     "/{id}/delete",
     *     name="post_delete",
     *     requirements={
     *         "id"="\d+"
     *     },
     *     methods={"DELETE"}
     * )
     * @param $id
     * @return mixed
     */
    public function deleteAction($id)
    {
        $post = $this->finderService->findOneById($id);

        if ($post == null) {
            throw $this->createNotFoundException();
        }

        $this->addFlash(
            'info',
            $this->translator->trans("flash_admin_post_delete %title%", ['%title%'=>$post['title']])
        );

        return $this->redirectToRoute('admin_post_index');
    }
}
